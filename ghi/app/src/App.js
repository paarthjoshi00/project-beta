import { BrowserRouter, Routes, Route } from 'react-router-dom';


import MainPage from './MainPage';
import Nav from './Nav';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';
import AppointmentsList from './AppointmentsList';
import AppointmentForm from './AppointmentForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import ManufacturerList from './ManufacturerList'
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList'
import ModelForm from './ModelForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import SalespersonHistory from './SalespersonHistory';
import ServiceHistory from './ServiceHistory';
import TechniciansList from './TechniciansList';
import TechnicianForm from './TechnicianForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>

          <Route path="/customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm  />} />
          </Route>

          <Route path="/salespeople">
            <Route index element={<SalespersonList />} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="/sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SalesForm /> } />
            <Route path="history" element={<SalespersonHistory />}/>
          </Route>

          <Route path="technicians/">
            <Route index element={<TechniciansList />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route index element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
