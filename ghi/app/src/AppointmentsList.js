import React, { useEffect, useState } from 'react';

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    function ifDoneOrCanceled(appointment) {
        if(appointment.status === "canceled" || appointment.status === "finished") {
            return true
        }
        return false
    }

    const getData = async() => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const appointmentResponse = await fetch(appointmentUrl);

        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();
            setAppointments(appointmentData.appointments);
            for (let appointment of appointmentData.appointments){
                const date = new Date(appointment.date_time).toLocaleDateString();
                appointment["date"] = date;
                const time =  new Date(appointment.date_time).toLocaleTimeString();
                appointment["time"] = time;
            }
        }

        const automobileUrl = 'http://localhost:8080/api/automobileVOs/'
        const automobileResponse = await fetch(automobileUrl);

        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            setAutomobiles(automobileData.AutomobileVOs);
        }
    }

    const handleCancel = async(id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel`, {'method': 'PUT'});
        if (response.ok){
            setAppointments(updatedAppointments => updatedAppointments.filter(appointment => appointment.id !== id));
        }

    }
    const handleFinish = async(id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish`, {'method': 'PUT'});
        if (response.ok){
            setAppointments(updatedAppointments => updatedAppointments.filter(appointment => appointment.id !== id));
        }}



    useEffect(() => {
        getData();
    }, [])


    let vins =[];
    {automobiles.map(automobile => {
        vins.push(automobile.vin)
    })}

    return (
        <div className="container">
          <h1 className="mb-3 mt-3">Appointments</h1>
          <table className='table table-striped'>
            <thead>
              <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
              </tr>
            </thead>
            <tbody>
                {appointments.map((appointment) => {
                    {if (!ifDoneOrCanceled(appointment)) {
                        {if (vins.includes(appointment.vin)) {
                            var vip = "Yes"
                        }
                        else {var vip ="No"}}
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{vip}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician.employee_id}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <button type="button" className="btn btn-danger" onClick={() => handleCancel(appointment.id)}>Cancel</button>
                                </td>
                                <td>
                                    <button type="button" className="btn btn-success" onClick={() => handleFinish(appointment.id)}>Finish</button>
                                </td>
                            </tr>
                        )
                    }}
                })}
            </tbody>
          </table>
        </div>
    )
}

export default AppointmentList;
