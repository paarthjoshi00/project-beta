import React, { useEffect, useState } from 'react';

function AppointmentForm() {
    const [vin, setVin] =  useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [reason, setReason] = useState('');
    const [state, setState] = useState(false);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = `${date}:${time}`
        data.technician = technician;
        data.reason = reason;

        const AppointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(AppointmentUrl, fetchConfig);
        if (response.ok) {
            console.log(data)
            setVin("");
            setCustomer("");
            setDate("");
            setTime("");
            setTechnician("");
            setReason("");
            setState(true);
        }
        else {
            setState(false);
        }
    };

    const handleVinChange = (event) => {
        setVin(event.target.value);
    }

    const handleCustomerChange = (event) => {
        setCustomer(event.target.value);
    }

    const handleDateChange = (event) => {
        setDate(event.target.value);
    }

    const handleTimeChange = (event) => {
        setTime(event.target.value);
    }

    const handleTechnicianChange = (event) => {
        setTechnician(event.target.value);
    }

    const handleReasonChange = (event) => {
        setReason(event.target.value);
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form id="create-appointment-form" onSubmit={handleSubmit}>
                        <h1>Create a service appointment</h1>
                        <div className="mb-3">
                            <label htmlFor="vin">Automobile Vin</label>
                            <input
                                onChange={handleVinChange}
                                required
                                type="text"
                                name="vin"
                                id="vin"
                                className="form-control"
                                value={vin}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="Customer">Customer</label>
                            <input
                                onChange={handleCustomerChange}
                                required
                                type="text"
                                name="customer"
                                id="customer"
                                className="form-control"
                                value={customer}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="date">Date</label>
                            <input
                                onChange={handleDateChange}
                                required
                                type="date"
                                name="date"
                                id="date"
                                className="form-control"
                                value={date}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="time">Time</label>
                            <input
                                onChange={handleTimeChange}
                                required
                                type="time"
                                name="time"
                                id="time"
                                className="form-control"
                                value={time}
                            />
                        </div>
                        <div className="mb-3"></div>
                            <label htmlFor="technician">Technician</label>
                            <select
                                onChange={handleTechnicianChange}
                                required
                                type=""
                                id="technician"
                                name="technician"
                                className="form-select"
                                value={technician}
                            >
                            <option value="">Choose a technician</option>
                            {technicians.map((technician) => (
                                <option value={technician.id} key={technician.id}>
                                    {technician.first_name} {technician.last_name}
                                </option>
                            ))}
                            </select>
                        <div className="mb-3">
                            <label htmlFor="reason">Reason</label>
                            <input
                                onChange={handleReasonChange}
                                required
                                type="text"
                                name="reason"
                                id="reason"
                                className="form-control"
                                value={reason}
                            />
                        </div>
                        <div>
                            <button className="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            {state?
                <div className="alert alert-success mb-0" id="success">
                    Successfully created appointment!
                </div>:<div></div>
            }
            </div>
        </div>
    )
}

export default AppointmentForm;
