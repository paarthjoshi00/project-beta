import React, { useEffect, useState } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');

    const getData = async() => {
        const data = 'http://localhost:8080/api/appointments/'
        const response = await fetch(data)

        if (response.ok) {
            const appointmentData = await response.json()
            setAppointments(appointmentData.appointments)
            for (let appointment of appointmentData.appointments){
                const date = new Date(appointment.date_time).toLocaleDateString();
                appointment["date"] = date;
                const time =  new Date(appointment.date_time).toLocaleTimeString();
                appointment["time"] = time;
            }
        }

        const automobileUrl = 'http://localhost:8080/api/automobileVOs/'
        const automobileResponse = await fetch(automobileUrl);

        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            setAutomobiles(automobileData.AutomobileVOs);
        }
    }

    useEffect(() => {
        getData();
    }, [])

    const handleSearch = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();

            for (let appointment of data.appointments){
                const date = new Date(appointment.date_time).toLocaleDateString();
                appointment["date"] = date;
                const time =  new Date(appointment.date_time).toLocaleTimeString();
                appointment["time"] = time;
            }
            setAppointments(updatedAppointments => updatedAppointments.filter(appointment => appointment.vin === searchTerm));
        }
        const automobileUrl = 'http://localhost:8080/api/automobileVOs/'
        const automobileResponse = await fetch(automobileUrl);
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            setAutomobiles(automobileData.AutomobileVOs);
        }
    }

    const handleSearchChange = (event) => {
        setSearchTerm(event.target.value)
    }


    let vins =[];
    {automobiles.map(automobile => {
        vins.push(automobile.vin)
    })}

    return (
        <>
            <div className="container">
            <h1 className="mb-3 mt-3">Service History</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map((appointment) => {
                            {if (vins.includes(appointment.vin)) {
                                var vip = "Yes"
                            }
                            else {var vip ="No"}}
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{vip}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.technician.employee_id}</td>
                                    <td>{appointment.reason}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <form className='col' onSubmit={handleSearch}>
                    <input className="form-control" id="searchbar" placeholder="Search by VIN" value={searchTerm} onChange={handleSearchChange}></input>
                    <button className="btn btn-primary">Search</button>
                </form>
                <button className="btn btn-success btn-sm" onClick={getData}>Refresh History</button>
            </div>
        </>
    )
}

export default ServiceHistory
