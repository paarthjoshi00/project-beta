import React, { useEffect, useState } from 'react';

function AutomobilesList() {
  const [automobiles, setAutomobiles] = useState([]);

  const getAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok){
      const data = await response.json();
      setAutomobiles(data.autos)
    }
  }

  useEffect(() => {
    getAutomobiles()
  }, []);

  const deleteAutomobile = async(id) => {
    const url = `http://localhost:8080/api/automobiles/${id}`
    const fetchConfig = {
        method: `DELETE`,
        headers: {
            'Content-type': 'application/json',
        },
    }
    try {
        const response = await fetch(url,fetchConfig)
        if (response.ok) {
            getAutomobiles()
        }
    } catch(error) {
        console.log('error deleting')
    }
}

  return (
    <>
      <h1 className="mb-3 mt-3">Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles?.map(automobile => (
            <tr key={automobile.vin}>
              <td>{automobile.vin}</td>
              <td>{automobile.color}</td>
              <td>{automobile.year}</td>
              <td>{automobile.model.name}</td>
              <td>{automobile.model.manufacturer.name}</td>
              <td>{automobile.sold ? 'Yes' : 'No'}</td>
              <td>
                <button onClick={() => deleteAutomobile(automobile.id)}>
                    Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default AutomobilesList;
