from django.db import models

# Create your models here.

class Technician(models.Model):
    first_name=models.CharField(max_length=100)
    last_name=models.CharField(max_length=100)
    employee_id=models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name} '

class AutomobileVO(models.Model):
    vin=models.CharField(max_length=100, unique=True)
    sold=models.BooleanField(default=False)

class Appointment(models.Model):
    date_time=models.DateTimeField()
    reason=models.CharField(max_length=100)
    status=models.CharField(max_length=100)
    vin=models.CharField(max_length=100)
    customer=models.CharField(max_length=100)

    technician=models.ForeignKey(
        Technician,
        related_name='appointments',
        on_delete=models.CASCADE,
    )

    def cancel(self):
        self.status = "canceled"
        self.save()

    def finish(self):
        self.status = "finished"
        self.save()

    def __str__(self):
        return f'{self.date_time} / {self.reason} / {self.customer}'
