from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from datetime import datetime
import json
# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model=AutomobileVO
    properties = ["vin","sold",]

class TechnicianListEncoder(ModelEncoder):
    model=Technician
    properties=["id", "first_name", "last_name", "employee_id", ]

class TechnicianDetailEncoder(ModelEncoder):
    model=Technician
    properties=["id", "first_name", "last_name", "employee_id", ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties=["id", "date_time", "reason", "status", "vin", "customer", "technician",]
    encoders = {"technician":TechnicianDetailEncoder()}

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties=["id", "date_time", "reason", "status", "vin", "customer", "technician", ]
    encoders = {"technician":TechnicianDetailEncoder()}


require_http_methods(["GET","POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians":technicians}, TechnicianListEncoder)
    else:
        content=json.loads(request.body)
        technician=Technician.objects.create(**content)
        return JsonResponse(technician, TechnicianListEncoder, safe=False)

require_http_methods(["GET", "DELETE"])
def api_technician_details(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(technician, TechnicianDetailEncoder, safe=False)
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})


require_http_methods(["GET","POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments":appointments}, AppointmentListEncoder)
    else:
        content=json.loads(request.body)
        try:
            technician_id = content['technician']
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"error": "Invalid employee_id"}, status=400)

        appointment = Appointment.objects.update_or_create(**content)
        return JsonResponse(appointment, AppointmentListEncoder, safe=False)


require_http_methods(["GET", "DELETE"])
def api_appointment_details(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(appointment, AppointmentDetailEncoder, safe=False)
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})

require_http_methods(["PUT"])
def api_cancel_appointment(request,pk):
    appointment=Appointment.objects.get(id=pk)
    appointment.cancel()
    return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)

require_http_methods(["PUT"])
def api_finished_appointment(request,pk):
    appointment=Appointment.objects.get(id=pk)
    appointment.finish()
    return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)

@require_http_methods(["GET"])
def api_get_automobileVOs(request):
    if request.method == "GET":
        automobileVOs = AutomobileVO.objects.all()
        return JsonResponse({"AutomobileVOs": automobileVOs},AutomobileVOEncoder,safe=False,)
