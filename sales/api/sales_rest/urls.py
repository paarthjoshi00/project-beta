from django.urls import path
from .views import (api_list_salespeople, api_show_salesperson, api_show_customer, api_list_customers, api_show_sale, api_list_sales, api_list_automobileVOs)

urlpatterns = [
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salesperson/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:id>/", api_show_customer, name="api_show_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sale/<int:id>/", api_show_sale, name="api_show_sale"),

    path("automobileVOs/", api_list_automobileVOs, name="api_list_automobileVOs")
]
